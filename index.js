'use strict'


const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')
const app = express()
const token = "EAAFD8EP18RQBAO83p800YEzIFikY2CLZB1yhgtQeBHw36nWgMSgnjRsUxhxCbwMgmiUxsbQWnV0sWOvwynkx549BAaDvDZAHodyPmplTwJBEcmMquJK21klWezMzT92LxY1Q1bmZABvVP8dIxx9TLqNxWFEGsTJYAqqBEG3eQZDZD"



app.set('port', (process.env.PORT || 5000))

// Process application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))

// Process application/json
app.use(bodyParser.json())

// Index route
app.get('/', function (req, res) {
	res.send('This is a chat bot designed for Indonesian Goverments Informations 52')
})

// for Facebook verification
app.get('/webhook/', function (req, res) {
	if (req.query['hub.verify_token'] === 'verified') {
		res.send(req.query['hub.challenge'])
	}
	res.send('Error, wrong token')
})

// Spin up the server
app.listen(app.get('port'), function() {
	console.log('running on port', app.get('port'))
})

app.post('/webhook/', function (req, res) {
    let messaging_events = req.body.entry[0].messaging
    for (let i = 0; i < messaging_events.length; i++) {
	    let event = req.body.entry[0].messaging[i]
	    let sender = event.sender.id
	    if (event.message && event.message.text) {
			let text = event.message.text
			

			if(text == 'hai') {
				sendTextMessage(sender,"Halo :)\r\nSelamat datang di BOT egovbench-indonesia\r\n untuk bantuan ketik 'help'");
			} else if (text == 'help') {
				sendTextMessage(sender,"Selamat datang di menu bantuan\r\nuntuk mencari informasi terkait pemerintahan di indonesia dapat menggunakan format '<atribut>,<nama_daerah>' misal pemimpin,Jawa Timur \r\n Pastikan tidak ada spasi setelah koma :) \r\n untuk melihat daftar atribut silahkan ketik atribut");
			} else if (text == 'atribut') {
				sendTextMessage(sender, "Daftar Atribut : \r\n1. Visi\r\n2. Motto\r\n3. Sejarah\r\n4. Pemimpin\r\n5. Lokasi\r\n6. Website\r\n7. Facebook\r\n8. Twitter\r\n9.Youtube")
			}

			else if(text.indexOf(",") !== -1 ) {
				
				var arr = text.toString().split(",");
				sendTextMessage(sender, "Input adalah " + arr[0] + " dari pemerintahan " + arr[1]);
				
			//	var changeCase = require("change-case");
				var pemda = arr[1].toUpperCase();

				var url = 'http://pesantren.santri.me:3030/ds/sparql?query=PREFIX+dbp%3A+%3Chttp%3A%2F%2Fdbpedia.org%2Fproperty%2F%3E%0D%0APREFIX+egb%3A+++%3Chttp%3A%2F%2Fpesantren.santri.me%3A3030%2Fegovbench-indonesia%2F%3E%0D%0APREFIX+foaf%3A++%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+geo%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23%3E%0D%0A%0D%0ASELECT+*+WHERE+%7B%0D%0A%3Fs+foaf%3Aname+%22'+pemda+'%22+.%0D%0AOPTIONAL+%7B%3Fs+foaf%3Ahomepage+%3Fsitus+%7D%0D%0AOPTIONAL+%7B%3Fs+dbp%3Amotto+%3Fmotto+%7D%0D%0AOPTIONAL+%7B%3Fs+dbp%3AleaderName+%3Fpemimpin+%7D%0D%0AOPTIONAL+%7B%3Fs+geo%3Alat+%3Flatitude+%7D%0D%0AOPTIONAL+%7B%3Fs+geo%3Along+%3Flongitude+%7D%0D%0AOPTIONAL+%7B%3Fs+egb%3AfbPemda+%3Ffacebook+%7D%0D%0AOPTIONAL+%7B%3Fs+egb%3AtwPemda+%3Ftw+%7D%0D%0AOPTIONAL+%7B%3Fs+egb%3AytPemda+%3Fyt+%7D%0D%0AOPTIONAL+%7B%3Fs+egb%3AvisiMisi+%3Fvisi+%7D%0D%0AOPTIONAL+%7B%3Fs+egb%3Asejarah+%3Fsej+%7D%0D%0A%7D&default-graph-uri=&output=json&stylesheet=%2Fxml-to-html.xsl';
				var request = require("request")

				request({
				    url: url,
				    json: true
				}, function (error, response, body) {

					//var jml = Object.keys(body.results.length);
					//var t = false;

					
					if(Object.keys(body.results.bindings).length !== 0) {
						
						
							//sendTextMessage(sender, "asdsd");

							if(arr[0] == 'Visi' || arr[0] == 'visi' ) {
								if('visi' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].visi.value);
									//sendTextMessage(sender, pemda);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}
							

							} else if(arr[0] == 'Website' || arr[0] == 'website' || arr[0] == 'situs') {

								if('situs' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].situs.value);
									//sendTextMessage(sender, pemda);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}
								
								
							} else if(arr[0] == 'Motto' || arr[0] == 'motto') {

								if('motto' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].motto.value);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}
								
							} else if(arr[0] == 'Pemimpin' || arr[0] == 'pemimpin') {

								if('pemimpin' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].pemimpin.value);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}

								
							} else if(arr[0] == 'Sejarah' || arr[0] == 'sejarah') {

								if('sej' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].sej.value.substring(0,200));
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}

								
							} else if(arr[0] == 'posisi' || arr[0] == 'lokasi') {

								if('latitude' in body.results.bindings[0]){
									sendTextMessage(sender, "Latitude " + body.results.bindings[0].latitude.value);
									sendTextMessage(sender, "Longitude " + body.results.bindings[0].longitude.value);
									
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}

								
							} else if(arr[0] == 'fb' || arr[0] == 'facebook') {

								if('facebook' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].facebook.value);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}
								
							} else if(arr[0] == 'twitter') {

								if('tw' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].tw.value);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}

								
							} else if(arr[0] == 'youtube') {

								if('yt' in body.results.bindings[0]){
									sendTextMessage(sender, body.results.bindings[0].yt.value);
								} else {
									sendTextMessage(sender, "Maaf "+arr[0]+" untuk "+arr[1]+" Belum Tersedia");
								}

								
							} else {
								sendTextMessage(sender, "Info mengenai " + arr[0] +" dari " + arr[1] + " Belum Tersedia");
							}
						
					} else {
						sendTextMessage(sender, "Info mengenai pemda tersebut belum tersedia");
					}

					
				    
				        
				});

				//sendTextMessage(sender, "Input sesuai format");

			} else {
				sendTextMessage(sender, "Input tidak sesuai format, ketik 'help' untuk bantuan");


			}
	    }
    }
    res.sendStatus(200)
})

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

function getJSONP(url, success) {

    var ud = '_' + +new Date,
        script = document.createElement('script'),
        head = document.getElementsByTagName('head')[0] 
               || document.documentElement;

    window[ud] = function(data) {
        head.removeChild(script);
        success && success(data);
    };

    script.src = url.replace('callback=?', 'callback=' + ud);
    head.appendChild(script);

}

function sendTextMessage(sender, text) {
    let messageData = { text:text }
    request({
	    url: 'https://graph.facebook.com/v2.6/me/messages',
	    qs: {access_token:token},
	    method: 'POST',
		json: {
		    recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
		    console.log('Error sending messages: ', error)
		} else if (response.body.error) {
		    console.log('Error: ', response.body.error)
	    }
    })
}

// var arr = text.toString().split(",");
// 				sendTextMessage(sender, "Input adalah " + arr[0] + "dari pemerintahan" + arr[1]);
				

// 				var url = 'http://pesantren.santri.me:3030/ds/sparql?query=SELECT * WHERE { ?s <http://pesantren.santri.me:3030/egovbench-indonesia/namaPemda> %22Jawa Barat%22	. ?s <http://pesantren.santri.me:3030/egovbench-indonesia/namaPemda> ?nama . ?s <http://pesantren.santri.me:3030/egovbench-indonesia/visiMisi> ?visi . ?s <http://pesantren.santri.me:3030/egovbench-indonesia/urlPemda> ?URL . ?s <http://pesantren.santri.me:3030/egovbench-indonesia/visiMisi> ?visi. ?s <http://pesantren.santri.me:3030/egovbench-indonesia/mottoDaerah> ?motto. ?s <http://pesantren.santri.me:3030/egovbench-indonesia/leaderName> ?pemimpin. ?s <http://pesantren.santri.me:3030/egovbench-indonesia/sejarah> ?sejarah . }&format=application%2Fsparql-results%2Bjson';
// 				var request = require("request")

// 				request({
// 				    url: url,
// 				    json: true
// 				}, function (error, response, body) {

// 					if(arr[0] == 'Visi' || arr[0] == 'visi' ) {
// 						sendTextMessage(sender, body.results.bindings[0].visi.value);

// 					} else if(arr[0] == 'Nama' || arr[0] == 'nama') {
// 						sendTextMessage(sender, body.results.bindings[0].nama.value);
// 					} else if(arr[0] == 'Website' || arr[0] == 'website') {
// 						sendTextMessage(sender, body.results.bindings[0].URL.value);
// 						//sendTextMessage(sender, Object.keys(body.results.bindings[0]).length);
// 					} else if(arr[0] == 'Motto' || arr[0] == 'motto') {
// 						sendTextMessage(sender, body.results.bindings[0].motto.value);
// 					} else if(arr[0] == 'Pemimpin' || arr[0] == 'pemimpin') {
// 						sendTextMessage(sender, body.results.bindings[0].pemimpin.value);
// 					} else if(arr[0] == 'Sejarah' || arr[0] == 'sejarah') {
// 						sendTextMessage(sender, body.results.bindings[0].sejarah.value);
// 					} else {
// 						sendTextMessage(sender, "Info mengenai " + arr[0] +" dari" + arr[1] + " Belum Tersedia");
// 					}
				    
				        
// 				});

//-----------------------

// if (text == "help") {
		
// 				        //    sendTextMessage(sender, "The bot is still under construction 1")
// 				          //  sendTextMessage(sender, "The bot is still under construction 2")

// 		          var fs = require("fs");

// 				fs.readFile('input.txt', function (err, data) {
// 				   if (err){
// 				      console.log(err.stack);
// 				      return;
// 				   }
// 				   //console.log(data.toString());
// 				   sendTextMessage(sender, data.toString());
// 				});
// 				//console.log("Program Ended");
				
				  
				
// 			}

// 			if (text == "oi") {
// 				//var t = '{ "name":"adr","age":"11"}';
// 				var fs = require("fs");
// 				var t = fs.readFileSync('jajal.json');
// 				var k = JSON.parse(t);
// 				//var g = k["1"]["name"];

// 				for(var key in k) {
// 					sendTextMessage(sender, "Urutan" + key + ", Nama : " + k[key].name + ", Umur " + k[key].age);
// 				}
				
// 			}

// 			else {
// 		    sendTextMessage(sender, "Pesan diterima: " + text.substring(0, 200))
// 			}